package com.chenpob.bmicalculate

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.Matchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

class CalculatorTests {
    @get:Rule()
    val activity = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun calculate_bmi() {
        onView(withId(R.id.weight_edit_text))
            .perform(typeText("60.00"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.height_edit_text))
            .perform(typeText("170.00"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.calculate))
            .perform(click())

        onView(withId(R.id.bmi))
            .check(matches(withText(containsString("20.8"))))

        onView(withId(R.id.people_shape))
            .check(matches(withText(containsString("Normal"))))
    }
}