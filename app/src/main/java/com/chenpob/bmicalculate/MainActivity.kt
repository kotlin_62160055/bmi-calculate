package com.chenpob.bmicalculate

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.chenpob.bmicalculate.databinding.ActivityMainBinding
import java.text.DecimalFormat
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculate.setOnClickListener { calculateBmi()
            binding.weightEditText.setOnKeyListener{ view, keycode, _ -> handleKeyEvent(view, keycode)}
            binding.heightEditText.setOnKeyListener{ view, keycode, _ -> handleKeyEvent(view, keycode)}
        }

    }

    private fun displayBmi(bmi: Double){
        val df = DecimalFormat("#.#")
        val formatBmi = df.format(bmi)
        binding.bmi.text = getString(R.string.bmi_people, formatBmi)

        if (formatBmi.toDouble() < 18.5){
            binding.peopleShape.text = "Underweight"
        }else if (formatBmi.toDouble() < 24.9){
            binding.peopleShape.text = "Normal"
        }else if (formatBmi.toDouble() < 29.9){
            binding.peopleShape.text = "Overweight"
        }else{
            binding.peopleShape.text = "Obese"
        }

    }


    private fun calculateBmi() {
        val weightTextField = binding.weightEditText.text.toString()
        val weightPeople = weightTextField.toDoubleOrNull()

        val heightTextField = binding.heightEditText.text.toString()
        val heightPeople = heightTextField.toDoubleOrNull()

        if(weightPeople == 0.00 || weightPeople == null
            || heightPeople == 0.00 || heightPeople == null){
            displayBmi(0.00)
            return
        }else{
            var bmiPeople = weightPeople / ((heightPeople/100) * (heightPeople/100))
            displayBmi(bmiPeople)
        }

    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }


}